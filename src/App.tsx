import React, { useEffect, useState } from "react";
import "./App.css";
import Header from "./components/header";
import GarbageLand from "./components/garbageLand";
import GarbageWater from "./components/garbageWater";
import Nature from "./components/nature";
import styled from "styled-components";
import tw from "twin.macro";
import TestAudio from "./components/testAudio";

export enum IGarbageType {
  Huuco = "Huu co",
  Voco = "Vo co",
  Taiche = "Tai che",
}

export interface DragItem {
  id: string;
  content: string;
  type: IGarbageType;
  image: string;
}
function App() {
  const [onLand, setOnland] = useState(true);
  const [nature, setIsNature] = useState(false);

  return (
    <div className="App">
      <Header />
      <section
        className="flex w-[50%] mx-auto flex-1 pt-20 justify-center"
        id="folders"
      >
        <article onClick={() => setIsNature(true)} className="text-center">
          Nature
        </article>
      </section>
      <Nature />
      {/* <TestAudio /> */}
    </div>
  );
}

export default App;
