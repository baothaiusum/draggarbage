import React from "react";
import "./styles.css";
const Header = () => {
  return (
    <div className="header">
      <div className="info">
        <h1 className="text-3xl">Giáo dục bảo vệ môi trường cho trẻ mầm non</h1>
      </div>
    </div>
  );
};

export default Header;
