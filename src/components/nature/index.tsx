import React, { useCallback, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import tw from "twin.macro";
import "./styles.css";
import buico from "../../assets/images/ranvacay/buico.png";
import ran from "../../assets/images/ranvacay/ran.png";
import cucgo from "../../assets/images/ranvacay/kechuyen.png";
import connguoi from "../../assets/images/ranvacay/BAICO.png";
import baicotrong from "../../assets/images/ranvacay/baicotrong.png";
import { Pages } from "../../assets/images/sachonline";
import Huuco from "../../assets/images/huuco";
import Taiche from "../../assets/images/taiche";
import Voco from "../../assets/images/voco";
import trap from "../../assets/images/objects";
import { toast } from "react-toastify";
import ranvacay from "../../assets/images/ranvacay";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";
import { log } from "console";
import ReactAudioPlayer from "react-audio-player";
const Nature = () => {
  const [playAudio, setPlayAudio] = useState(false);
  const [pageDisplay, setPageDisplay] = useState(1);
  const [displayGrassSelected, setDisplayGrassSelected] = useState(false);
  useEffect(() => {
    const audio = new Audio("http://streaming.tdiradio.com:8000/house.mp3");
    if (playAudio) {
      console.log({ audio });
      audio.play();
    } else {
      audio.pause();
      audio.currentTime = 0;
      console.log({ audio });
    }
    console.log(playAudio);
  }, [playAudio]);

  const prevPage = () => {
    if (pageDisplay <= 1) {
      setPageDisplay(1);
    } else setPageDisplay(pageDisplay - 1);
  };

  const nextPage = () => {
    if (pageDisplay >= Pages.length) {
      setPageDisplay(1);
    } else setPageDisplay(pageDisplay + 1);
  };

  //garbage land
  const garbageItemsLand = [...ItemsLand, ...ItemsLand2, ...ItemsLand3];
  const garbageItemsWater = [...ItemsWater, ...ItemsWater2, ...ItemsWater3];
  const [garbageHover, setGarbageHover] = useState<
    {
      id: number;
      image: string;
      isClick: boolean;
    }[]
  >(hoverItems);
  const [itemsLand, setItemsLand] = useState<DragItem[]>(garbageItemsLand);
  const [itemsWater, setItemsWater] = useState<DragItem[]>(garbageItemsWater);

  const [itemDragged, setItemDragged] = useState<DragItem | null>(null);
  const [removeSuccess, setRemoveSuccess] = useState<boolean>(false);

  //state for traps
  const [trap1, setTrap1] = useState(false);
  const [trap2, setTrap2] = useState(false);
  const [trap3, setTrap3] = useState(false);
  const [trap4, setTrap4] = useState(false);
  const [trap5, setTrap5] = useState(false);

  const [trapImg1, setTrapImg1] = useState(false);
  const [trapImg2, setTrapImg2] = useState(false);
  const [trapImg3, setTrapImg3] = useState(false);
  const [trapImg4, setTrapImg4] = useState(false);
  const [trapImg5, setTrapImg5] = useState(false);

  const [deltaPosition, setDeltaPosition] = useState<{ x: number; y: number }>({
    x: 0,
    y: 0,
  });

  const ref = useRef<HTMLDivElement>(null);

  const onDropLand = (typeGarbage: string) => {
    if (!itemsLand) return;
    if (typeGarbage === itemDragged?.type) {
      setItemsLand(
        itemsLand.filter(
          (item) => item !== itemDragged && item.id !== itemDragged.id
        )
      );
      setRemoveSuccess(true);
    }
  };

  const onDropWater = (typeGarbage: string) => {
    if (!itemsWater) return;
    if (typeGarbage === itemDragged?.type) {
      setItemsWater(
        itemsWater.filter(
          (item) => item !== itemDragged && item.id !== itemDragged.id
        )
      );
      setRemoveSuccess(true);
    }
  };

  const onDragStartLand = (item: DragItem) => {
    setItemDragged(item);
  };

  const onDragStartWater = (item: DragItem) => {
    setItemDragged(item);
  };

  // on drag for draggable
  const handleDrag = (e: DraggableEvent, ui: DraggableData) => {
    console.log(ui);
    const { x, y } = deltaPosition;
    setDeltaPosition({ x: x + ui.deltaX, y: y + ui.deltaY });
  };

  useEffect(() => {
    console.log(deltaPosition.x + " " + deltaPosition.y);
  }, [deltaPosition]);

  useEffect(() => {
    if (removeSuccess) {
      toast.success("Đúng rồi bé ơi");
      setRemoveSuccess(false);
    }
  }, [removeSuccess]);

  return (
    <>
      <div className="flex "></div>
      <div className="relative max-w-[1000px] mx-auto ">
        <button
          className="absolute bg-slate-800 text-white w-[50px] h-[50px] rounded-[50%] top-[350px] -left-12"
          onClick={() => prevPage()}
        >
          Prev
        </button>
        {Pages.map((item) => {
          if (item.id === 7) {
            return (
              <ParentDivGarBage
                className={`${pageDisplay !== item.id ? "opacity-0 " : ""}`}
                img={item.page}
              >
                <div className="grid-container  z-10 relative ">
                  <div className="mainArea absolute ">
                    <div className="flex flex-wrap ">
                      {itemsLand.map((item) => (
                        <div
                          onDragStart={(e) => onDragStartLand(item)}
                          draggable
                          className="z-10"
                        >
                          <img src={item.image} width={120} alt="" />
                        </div>
                      ))}
                    </div>
                  </div>
                  <div
                    className={`grid-container-trashcan absolute right-4 top-[60px] `}
                  >
                    <div
                      onDragOver={(e) => e.preventDefault()}
                      onDrop={(e) => onDropLand(IGarbageType.Taiche)}
                      className="z-10 pb-24 w-60 firstTrashcanArea"
                    >
                      <img src={Taiche.thungractaiche} alt="" />
                    </div>
                    <div
                      className="z-10 w-60 fourthTrashcanArea"
                      onDragOver={(e) => e.preventDefault()}
                      onDrop={(e) => onDropLand(IGarbageType.Huuco)}
                    >
                      <img src={Huuco.thungRacHuuCo} alt="" />
                    </div>
                    <div
                      onDragOver={(e) => e.preventDefault()}
                      onDrop={(e) => onDropLand(IGarbageType.Voco)}
                      className="z-10 w-60  thirdTrashcanArea"
                    >
                      <img src={Voco.thungracvoco} alt="" />
                    </div>
                  </div>
                </div>
              </ParentDivGarBage>
            );
          } else if (item.id === 10) {
            return (
              <>
                <ParentDiv2
                  // src={item.page}
                  img={item.page}
                  className={`${pageDisplay !== item.id ? "opacity-0 " : ""}`}
                >
                  <div className="relative">
                    <TrashHover
                      isClick={garbageHover[0].isClick}
                      onClick={() => {
                        const findGarbage = garbageHover.find(
                          (item) => item.id === garbageHover[0].id
                        );
                        if (!findGarbage) {
                          return;
                        }
                        if (findGarbage) {
                          findGarbage.isClick = !findGarbage.isClick;
                        }

                        setGarbageHover([...garbageHover, findGarbage]);
                      }}
                      image={garbageHover[0].image}
                      className="absolute z-10 trashHover w-[120px] h-[120px]  top-10 right-60"
                    ></TrashHover>
                    <TrashHover
                      isClick={garbageHover[1].isClick}
                      onClick={() => {
                        const findGarbage = garbageHover.find(
                          (item) => item.id === garbageHover[1].id
                        );
                        if (!findGarbage) {
                          return;
                        }
                        if (findGarbage) {
                          findGarbage.isClick = !findGarbage.isClick;
                        }

                        setGarbageHover([...garbageHover, findGarbage]);
                      }}
                      image={garbageHover[1].image}
                      className="absolute z-10 trashHover w-[120px] h-[120px]  top-[360px] right-60"
                    ></TrashHover>
                    <TrashHover
                      isClick={garbageHover[2].isClick}
                      onClick={() => {
                        const findGarbage = garbageHover.find(
                          (item) => item.id === garbageHover[2].id
                        );
                        if (!findGarbage) {
                          return;
                        }
                        if (findGarbage) {
                          findGarbage.isClick = !findGarbage.isClick;
                        }

                        setGarbageHover([...garbageHover, findGarbage]);
                      }}
                      image={garbageHover[2].image}
                      className="absolute z-10 trashHover w-[120px] h-[120px]  top-20 right-0"
                    ></TrashHover>
                    <TrashHover
                      isClick={garbageHover[3].isClick}
                      onClick={() => {
                        const findGarbage = garbageHover.find(
                          (item) => item.id === garbageHover[3].id
                        );
                        if (!findGarbage) {
                          return;
                        }
                        if (findGarbage) {
                          findGarbage.isClick = !findGarbage.isClick;
                        }

                        setGarbageHover([...garbageHover, findGarbage]);
                      }}
                      image={garbageHover[3].image}
                      className="absolute z-10 trashHover w-[120px] h-[120px]  top-[360px] right-0"
                    ></TrashHover>
                  </div>
                </ParentDiv2>
              </>
            );
          } else if (item.id === 11) {
            return (
              <ParentDivGarBage
                img={item.page}
                className={`${pageDisplay !== item.id ? "opacity-0 " : ""}`}
              >
                <div className="background relative">
                  <div className="relative top-[120px]">
                    <div className="flex justify-around items-center gap-20">
                      <div className=""></div>
                      <div className=""></div>
                      <div className="flex   items-center gap-2">
                        <div
                          onDragOver={(e) => e.preventDefault()}
                          onDrop={(e) => onDropWater(IGarbageType.Taiche)}
                          className="w-[110px] z-10 flex flex-col items-center"
                        >
                          <img src={Taiche.thungractaiche} alt="" />
                          <h3 className="text-white">Tái chế</h3>
                        </div>
                        <div
                          className="w-[110px]  z-10 flex flex-col items-center"
                          onDragOver={(e) => e.preventDefault()}
                          onDrop={(e) => onDropWater(IGarbageType.Huuco)}
                        >
                          <img src={Huuco.thungRacHuuCo} alt="" />
                          <h3 className="text-white">Hữu cơ</h3>
                        </div>
                        <div
                          className="w-[110px] z-10  flex flex-col items-center "
                          onDragOver={(e) => e.preventDefault()}
                          onDrop={(e) => onDropWater(IGarbageType.Voco)}
                        >
                          <img src={Voco.thungracvoco} alt="" />
                          <h3 className="text-white">Vô cơ</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="pt-60 grid-containerWater gap-2">
                    <div className="flex flex-wrap gap-1 mr-20">
                      {itemsWater.map((item) => {
                        function getRandom(
                          min: number,
                          max: number,
                          step: number
                        ) {
                          return Math.floor(
                            Math.random() * (max - min) + min + step
                          );
                        }
                        const nums = getRandom(5, 30, 10);
                        if (Number(item.id) <= 4)
                          return (
                            <div
                              onDragStart={(e) => onDragStartWater(item)}
                              draggable
                              className="z-10 leftArea"
                            >
                              <IMGAnimation
                                src={item.image}
                                className="elementAnimation"
                                width={50}
                                height={50}
                                alt=""
                                numRandom={nums}
                              />
                            </div>
                          );
                      })}
                    </div>
                    <div className="flex flex-wrap gap-2 ml-20">
                      {itemsWater.map((item) => {
                        function getRandom(
                          min: number,
                          max: number,
                          step: number
                        ) {
                          return Math.floor(
                            Math.random() * (max - min) + min + step
                          );
                        }
                        const nums = getRandom(5, 40, 10);
                        if (Number(item.id) >= 5 && Number(item.id) <= 8)
                          return (
                            <div
                              onDragStart={(e) => onDragStartWater(item)}
                              draggable
                              className="z-10 rightArea"
                            >
                              <IMGAnimation
                                src={item.image}
                                className="elementAnimation"
                                width={50}
                                height={50}
                                alt=""
                                numRandom={nums}
                              />
                            </div>
                          );
                      })}
                    </div>
                    <div className="flex flex-wrap gap-2 ml-20">
                      {itemsWater.map((item) => {
                        function getRandom(
                          min: number,
                          max: number,
                          step: number
                        ) {
                          return Math.floor(
                            Math.random() * (max - min) + min + step
                          );
                        }
                        const nums = getRandom(5, 30, 10);
                        if (Number(item.id) >= 9 && Number(item.id) <= 13)
                          return (
                            <div
                              onDragStart={(e) => onDragStartWater(item)}
                              draggable
                              className="z-10 right2Area"
                            >
                              <IMGAnimation
                                src={item.image}
                                className="elementAnimation"
                                width={50}
                                height={50}
                                alt=""
                                numRandom={nums}
                              />
                            </div>
                          );
                      })}
                    </div>
                    <div className="flex flex-wrap gap-2 ml-20">
                      {itemsWater.map((item) => {
                        function getRandom(
                          min: number,
                          max: number,
                          step: number
                        ) {
                          return Math.floor(
                            Math.random() * (max - min) + min + step
                          );
                        }
                        const nums = getRandom(5, 40, 10);
                        if (Number(item.id) >= 14 && Number(item.id) <= 18)
                          return (
                            <div
                              onDragStart={(e) => onDragStartWater(item)}
                              draggable
                              className="z-10 left2Area"
                            >
                              <IMGAnimation
                                src={item.image}
                                className="elementAnimation"
                                width={50}
                                height={50}
                                alt=""
                                numRandom={nums}
                              />
                            </div>
                          );
                      })}
                    </div>
                  </div>
                </div>
              </ParentDivGarBage>
            );
          } else if (item.id === 14) {
            return (
              <>
                <ParentDiv2
                  img={item.page}
                  className={`${pageDisplay !== item.id ? "opacity-0 " : ""} `}
                >
                  <ReactAudioPlayer
                    style={{
                      position: "relative",
                      zIndex: 100,
                    }}
                    src="https://www.bensound.com/bensound-music/bensound-memories.mp3"
                    controls
                  />
                  <div className=" treeArea relative ">
                    <ChildDivContainer
                      isClick={displayGrassSelected}
                      img={buico}
                      onClick={() =>
                        setDisplayGrassSelected(!displayGrassSelected)
                      }
                    >
                      {/* className="bg-red-600 absolute top-20 left-0 w-[100%] h-[300px]" */}

                      <ChildDiv
                        img={cucgo}
                        isClick={false}
                        className={`${
                          !displayGrassSelected
                            ? "invisible opacity-0"
                            : "visible opacity-100"
                        } absolute top-[20%] duration-300 left-[20%] w-[300px] h-[150px]`}
                      ></ChildDiv>
                      <ChildDiv
                        isClick={false}
                        img={ran}
                        className={`${
                          !displayGrassSelected
                            ? "invisible opacity-0"
                            : "visible opacity-100"
                        }   absolute top-[40%] left-[30%] w-[200px] duration-300 h-[150px]`}
                      ></ChildDiv>
                    </ChildDivContainer>
                  </div>
                  <div className=" treeArea relative z-50 w-[100%] h-[100px]">
                    <div className="flex absolute bottom-20 w-[100%]">
                      {Array(6)
                        .fill(0)
                        .map((item, index) => {
                          return (
                            <Draggable
                              axis="both"
                              // bounds={"body"}
                              onDrag={handleDrag}
                            >
                              <ChildDiv
                                img={ranvacay.cay}
                                className="z-50 w-[150px]  h-[100px]"
                              ></ChildDiv>
                            </Draggable>
                          );
                        })}
                    </div>

                    {/*
                  <div className="z-50 w-[100px] h-[100px] top-10 left-[90px] absolute bg-black"></div>
                  <div className="z-50 w-[100px] h-[100px] top-[180px] left-[90px] absolute bg-red-500"></div>
                  <div className="z-50 w-[100px] h-[100px] top-80 left-[60px] absolute bg-blue-500"></div>
                  <div className="z-50 w-[100px] h-[100px] top-[450px] left-[60px] absolute bg-yellow-500"></div>
                  <div className="z-50 w-[100px] h-[100px] top-[40px] right-[40px]  absolute bg-yellow-500"></div>
                  <div className="z-50 w-[100px] h-[100px] top-[180px] right-[40px]  absolute bg-red-500"></div>
                  <div className="z-50 w-[100px] h-[100px] top-[320px] right-[40px]  absolute bg-black"></div>
                  <div className="z-50 w-[100px] h-[100px] top-[450px] right-[60px]  absolute bg-purple-500"></div> */}
                  </div>
                </ParentDiv2>
              </>
            );
          } else if (item.id === 17) {
            return (
              <>
                <ParentDiv2
                  // src={item.page}
                  img={item.page}
                  className={`${
                    pageDisplay !== item.id ? "opacity-0 " : ""
                  } relative`}
                >
                  <BackgroundDiv
                    onClick={() => setTrap1(true)}
                    img={trap1 ? "" : trap.bui1}
                    className={
                      "absolute h-[150px] z-10 cursor-pointer transform duration-200   w-[400px] bottom-0 left-0"
                    }
                  >
                    {trap1 && !trapImg1 && (
                      <img
                        width={100}
                        className="absolute z-20  bottom-10 left-10 transform duration-200"
                        src={trap.bay}
                        alt=""
                        onClick={() => setTrapImg1(true)}
                      />
                    )}
                  </BackgroundDiv>
                  <BackgroundDiv
                    onClick={() => setTrap2(true)}
                    img={trap2 ? "" : trap.bui2}
                    className={
                      " absolute bottom-0 w-[500px]  z-10 cursor-pointer transform duration-200   h-[200px] right-[-65px]"
                    }
                  >
                    <div className="absolute  bottom-0 right-60 transform duration-200 z-20">
                      {trap2 && !trapImg2 && (
                        <img
                          width={100}
                          src={trap.bay}
                          alt=""
                          onClick={() => setTrapImg2(true)}
                        />
                      )}
                    </div>
                  </BackgroundDiv>
                  <BackgroundDiv
                    onClick={() => setTrap3(true)}
                    img={trap3 ? "" : trap.bui3}
                    className={
                      "absolute top-[50%] left-[50%] cursor-pointer  transform duration-200 z-10 h-[100px]   w-[250px] "
                    }
                  >
                    <div className="left-10 absolute transform duration-200 z-20">
                      {trap3 && !trapImg3 && (
                        <img
                          width={100}
                          src={trap.bay}
                          alt=""
                          onClick={() => {
                            setTrapImg3(true);
                          }}
                        />
                      )}
                    </div>
                  </BackgroundDiv>
                  <BackgroundDiv
                    onClick={() => setTrap4(true)}
                    img={trap4 ? "" : trap.bui4}
                    className="bg-red-600 absolute cursor-pointer top-[30%] z-10 transform duration-200  left-[15%] h-[100px]  w-[300px] "
                  >
                    <div className="absolute top-2 left-40 transform duration-200 z-20">
                      {trap4 && !trapImg4 && (
                        <img
                          width={90}
                          src={trap.bay}
                          alt=""
                          onClick={() => setTrapImg4(true)}
                        />
                      )}
                    </div>
                  </BackgroundDiv>
                  <BackgroundDiv
                    onClick={() => setTrap5(true)}
                    img={trap5 ? "" : trap.tancay}
                    // img={trap.tancay}
                    className="cursor-pointer absolute  h-[350px] w-[300px] transform duration-200 right-0 top-0 z-20"
                  >
                    <div className="absolute transform duration-200 right-20 top-20 z-20">
                      {trap5 && !trapImg5 && (
                        <img
                          width={90}
                          src={trap.bay}
                          alt=""
                          onClick={() => setTrapImg5(true)}
                        />
                      )}
                    </div>
                  </BackgroundDiv>
                </ParentDiv2>
              </>
            );
          } else
            return (
              <>
                <ParentDiv2
                  // src={item.page}
                  img={item.page}
                  className={`${pageDisplay !== item.id ? "opacity-0 " : ""}`}
                ></ParentDiv2>
              </>
            );
        })}
        <button
          className="absolute bg-slate-800  text-white w-[50px] h-[50px] rounded-[50%] top-[350px] -right-[65px]"
          onClick={() => nextPage()}
        >
          Next
        </button>
      </div>
    </>
  );
};

export default Nature;

const BackgroundDiv = styled.div<{ img: string }>`
  background: url(${({ img }) => img && `${img}`});
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: center;
`;

const ParentDiv2 = styled.div<{ img: string }>`
  ${tw`absolute transform duration-300 w-[1000px] h-[700px]  m-2  bg-red-500 `}
  background: url(${({ img }) => img && `${img}`});
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: center;
`;

const ParentDivGarBage = styled.div<{ img: string }>`
  ${tw`absolute transform duration-300 w-[1000px] h-[700px]  m-2  bg-red-500 `}
  background: url(${({ img }) => img && `${img}`});
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: center;
`;

const ParentDiv = styled.div<{ img: string }>`
  ${tw``}
  margin : 0 auto;
  position: relative;
  width: 700px;
  height: 800px;
  max-height: 900px;
  background: url(${({ img }) => img && `${img}`});
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: center;
`;
//transform: skew(${({ numRandom }) => numRandom && `${numRandom}deg`});
const ChildDivContainer = styled.div<{ isClick: boolean; img: string }>`
  background: url(${({ img }) => img && `${img}`});
  background-size: 100%;

  background-repeat: no-repeat;
  background-position: center;

  ${tw` absolute hover:cursor-pointer w-[100%] top-10 left-0.5 z-10  h-[400px]   duration-300  `}/* ${({
    isClick,
  }) =>
    isClick ? tw`opacity-0 invisible w-[0px] ` : tw` opacity-100 visible `} */
`;
const ChildDiv = styled.div<{ isClick?: boolean; img: string }>`
  background-image: url(${({ img }) => img && `${img}`});
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: center;
  /* ${tw`   opacity-0 invisible duration-300 w-[0px] h-[200px]  `}
  ${({ isClick }) => (isClick ? tw`opacity-100 visible w-[200px]   ` : tw``)} */
`;

enum IGarbageType {
  Huuco = "Huu co",
  Voco = "Vo co",
  Taiche = "Tai che",
}

interface DragItem {
  id: string;
  content: string;
  type: IGarbageType;
  image: string;
}
const ItemsLand: DragItem[] = [
  {
    id: "1",
    content: "Tao",
    type: IGarbageType.Huuco,
    image: Huuco.Tao,
  },
  {
    id: "2",
    content: "Vochuoi",
    type: IGarbageType.Huuco,
    image: Huuco.Vochuoi,
  },
  {
    id: "3",
    content: "Votrung",
    type: IGarbageType.Huuco,
    image: Huuco.Votrung,
  },
];
const ItemsLand2: DragItem[] = [
  {
    id: "4",
    content: "Chai",
    type: IGarbageType.Taiche,
    image: Taiche.chai,
  },
  {
    id: "5",
    content: "Chai nhua",
    type: IGarbageType.Taiche,
    image: Taiche.chainhua,
  },
  {
    id: "6",
    content: "Hop sua",
    type: IGarbageType.Taiche,
    image: Taiche.hopsua,
  },
  {
    id: "7",
    content: "Lon bia",
    type: IGarbageType.Taiche,
    image: Taiche.lonbia,
  },

  {
    id: "9",
    content: "Ly nhua",
    type: IGarbageType.Taiche,
    image: Taiche.lynhua,
  },
  {
    id: "10",
    content: "Rac1",
    type: IGarbageType.Taiche,
    image: Taiche.rac1,
  },
  {
    id: "11",
    content: "Thia",
    type: IGarbageType.Taiche,
    image: Taiche.thia,
  },
];
const ItemsLand3: DragItem[] = [
  {
    id: "12",
    content: "Chai do",
    type: IGarbageType.Voco,
    image: Voco.chaido,
  },
  {
    id: "13",
    content: "Chai ruou",
    type: IGarbageType.Voco,
    image: Voco.chairuou,
  },

  {
    id: "15",
    content: "Cuc giay",
    type: IGarbageType.Voco,
    image: Voco.cucgiay,
  },
  {
    id: "16",
    content: "Nilon",
    type: IGarbageType.Voco,
    image: Voco.nilon,
  },

  {
    id: "18",
    content: "Tui rac",
    type: IGarbageType.Voco,
    image: Voco.tuirac,
  },
];

//garbage water
const IMGAnimation = styled.img<{ numRandom: number }>`
  @keyframes skew {
    0% {
      transform: skewX(${({ numRandom }) => numRandom && `${numRandom}deg`});
    }
    100% {
      transform: skewX(${({ numRandom }) => numRandom && `-${numRandom}deg`});
    }
  }
  animation: skew 3s infinite;
  transform: skew(${({ numRandom }) => numRandom && `${numRandom}deg`});
  animation-direction: alternate-reverse;
`;

const ItemsWater: DragItem[] = [
  {
    id: "1",
    content: "Tao",
    type: IGarbageType.Huuco,
    image: Huuco.Tao,
  },
  {
    id: "2",
    content: "Vochuoi",
    type: IGarbageType.Huuco,
    image: Huuco.Vochuoi,
  },
  {
    id: "3",
    content: "Votrung",
    type: IGarbageType.Huuco,
    image: Huuco.Votrung,
  },
];
const ItemsWater2: DragItem[] = [
  {
    id: "4",
    content: "Chai",
    type: IGarbageType.Taiche,
    image: Taiche.chai,
  },
  {
    id: "5",
    content: "Chai nhua",
    type: IGarbageType.Taiche,
    image: Taiche.chainhua,
  },
  {
    id: "6",
    content: "Hop sua",
    type: IGarbageType.Taiche,
    image: Taiche.hopsua,
  },
  {
    id: "7",
    content: "Lon bia",
    type: IGarbageType.Taiche,
    image: Taiche.lonbia,
  },

  {
    id: "9",
    content: "Ly nhua",
    type: IGarbageType.Taiche,
    image: Taiche.lynhua,
  },
  {
    id: "10",
    content: "Rac1",
    type: IGarbageType.Taiche,
    image: Taiche.rac1,
  },
  {
    id: "11",
    content: "Thia",
    type: IGarbageType.Taiche,
    image: Taiche.thia,
  },
];
const ItemsWater3: DragItem[] = [
  {
    id: "12",
    content: "Chai do",
    type: IGarbageType.Voco,
    image: Voco.chaido,
  },
  {
    id: "13",
    content: "Chai ruou",
    type: IGarbageType.Voco,
    image: Voco.chairuou,
  },

  {
    id: "15",
    content: "Cuc giay",
    type: IGarbageType.Voco,
    image: Voco.cucgiay,
  },
  {
    id: "16",
    content: "Nilon",
    type: IGarbageType.Voco,
    image: Voco.nilon,
  },

  {
    id: "18",
    content: "Tui rac",
    type: IGarbageType.Voco,
    image: Voco.tuirac,
  },
];

const hoverItems = [
  {
    id: Math.random(),
    image: Voco.chairuou,
    isClick: false,
  },
  {
    id: Math.random(),
    image: Huuco.Vochuoi,
    isClick: false,
  },
  {
    id: Math.random(),
    image: Taiche.chainhua,
    isClick: false,
  },
  {
    id: Math.random(),
    image: Huuco.Votrung,
    isClick: false,
  },
];

const TrashHover = styled.div<{ isClick: boolean; image: string }>`
  ${tw``}
  background-image: url(${({ isClick, image }) => (isClick ? `${image}` : "")});
  background-size: 100%;

  background-repeat: no-repeat;
  background-position: center;
`;
