import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import styled from "styled-components";
import tw from "twin.macro";
import Huuco from "../../../assets/images/huuco";
import Taiche from "../../../assets/images/taiche";
import Voco from "../../../assets/images/voco";
const GarbageLand = () => {
  const garbageItems = [...Items, ...Items2, ...Items3];
  const [items, setItems] = useState<DragItem[]>(garbageItems);
  const [itemDragged, setItemDragged] = useState<DragItem | null>(null);
  const [removeSuccess, setRemoveSuccess] = useState<boolean>(false);
  const onDrop = (typeGarbage: string) => {
    if (!items) return;
    if (typeGarbage === itemDragged?.type) {
      setItems(
        items.filter(
          (item) => item !== itemDragged && item.id !== itemDragged.id
        )
      );
      setRemoveSuccess(true);
    }
  };

  const onDragStart = (item: DragItem) => {
    setItemDragged(item);
  };

  useEffect(() => {
    if (removeSuccess) {
      toast.success("Đúng rồi bé ơi");
      setRemoveSuccess(false);
    }
  }, [removeSuccess]);
  return <div>GarbageLand</div>;
};

export default GarbageLand;

const ParentDiv2 = styled.div<{ img: string }>`
  ${tw`absolute transform duration-300 w-[1000px] h-[700px]  m-2  bg-red-500 `}
  background: url(${({ img }) => img && `${img}`});
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: center;
`;

const ParentDivGarBage = styled.div<{ img: string }>`
  ${tw`absolute transform duration-300 w-[1000px] h-[700px]  m-2  bg-red-500 `}
  background: url(${({ img }) => img && `${img}`});
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: center;
`;

const ParentDiv = styled.div<{ img: string }>`
  ${tw``}
  margin : 0 auto;
  position: relative;
  width: 700px;
  height: 800px;
  max-height: 900px;
  background: url(${({ img }) => img && `${img}`});
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: center;
`;
//transform: skew(${({ numRandom }) => numRandom && `${numRandom}deg`});
const ChildDivContainer = styled.div<{ isClick: boolean; img: string }>`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 70%, #090909),
    url(${({ img }) => img && `${img}`});
  background-size: 100%;

  background-repeat: no-repeat;
  background-position: center;
  ${tw`   opacity-0 invisible duration-300 w-[0px] h-[200px]  `}
  ${({ isClick }) => (isClick ? tw`opacity-100 visible w-[200px]   ` : tw``)}
`;
const ChildDiv = styled.div<{ isClick: boolean; img: string }>`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-image: url(${({ img }) => img && `${img}`});
  background-size: 100%;

  background-repeat: no-repeat;
  background-position: center;
  ${tw`   opacity-0 invisible duration-300 w-[0px] h-[200px]  `}
  ${({ isClick }) => (isClick ? tw`opacity-100 visible w-[200px]   ` : tw``)}
`;

enum IGarbageType {
  Huuco = "Huu co",
  Voco = "Vo co",
  Taiche = "Tai che",
}

interface DragItem {
  id: string;
  content: string;
  type: IGarbageType;
  image: string;
}
const Items: DragItem[] = [
  {
    id: "1",
    content: "Tao",
    type: IGarbageType.Huuco,
    image: Huuco.Tao,
  },
  {
    id: "2",
    content: "Vochuoi",
    type: IGarbageType.Huuco,
    image: Huuco.Vochuoi,
  },
  {
    id: "3",
    content: "Votrung",
    type: IGarbageType.Huuco,
    image: Huuco.Votrung,
  },
];
const Items2: DragItem[] = [
  {
    id: "4",
    content: "Chai",
    type: IGarbageType.Taiche,
    image: Taiche.chai,
  },
  {
    id: "5",
    content: "Chai nhua",
    type: IGarbageType.Taiche,
    image: Taiche.chainhua,
  },
  {
    id: "6",
    content: "Hop sua",
    type: IGarbageType.Taiche,
    image: Taiche.hopsua,
  },
  {
    id: "7",
    content: "Lon bia",
    type: IGarbageType.Taiche,
    image: Taiche.lonbia,
  },

  {
    id: "9",
    content: "Ly nhua",
    type: IGarbageType.Taiche,
    image: Taiche.lynhua,
  },
  {
    id: "10",
    content: "Rac1",
    type: IGarbageType.Taiche,
    image: Taiche.rac1,
  },
  {
    id: "11",
    content: "Thia",
    type: IGarbageType.Taiche,
    image: Taiche.thia,
  },
];
const Items3: DragItem[] = [
  {
    id: "12",
    content: "Chai do",
    type: IGarbageType.Voco,
    image: Voco.chaido,
  },
  {
    id: "13",
    content: "Chai ruou",
    type: IGarbageType.Voco,
    image: Voco.chairuou,
  },

  {
    id: "15",
    content: "Cuc giay",
    type: IGarbageType.Voco,
    image: Voco.cucgiay,
  },
  {
    id: "16",
    content: "Nilon",
    type: IGarbageType.Voco,
    image: Voco.nilon,
  },

  {
    id: "18",
    content: "Tui rac",
    type: IGarbageType.Voco,
    image: Voco.tuirac,
  },
];
