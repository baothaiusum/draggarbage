import React from "react";

export default function TestAudio() {
  const [playAudio, setPlayAudio] = React.useState(false);

  return (
    <div>
      <h1>TestAudio</h1>
      <button
        onClick={() => setPlayAudio(!playAudio)}
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
      >
        Play{" "}
      </button>
    </div>
  );
}
