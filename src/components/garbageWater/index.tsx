import React, { useEffect, useState } from "react";
import Huuco from "../../assets/images/huuco/index";
import Voco from "../../assets/images/voco/index";
import Taiche from "../../assets/images/taiche/index";
import { toast } from "react-toastify";
import "./styles.css";
import styled from "styled-components";
import tw from "twin.macro";
enum IGarbageType {
  Huuco = "Huu co",
  Voco = "Vo co",
  Taiche = "Tai che",
}

interface DragItem {
  id: string;
  content: string;
  type: IGarbageType;
  image: string;
}
const Items: DragItem[] = [
  {
    id: "1",
    content: "Tao",
    type: IGarbageType.Huuco,
    image: Huuco.Tao,
  },
  {
    id: "2",
    content: "Vochuoi",
    type: IGarbageType.Huuco,
    image: Huuco.Vochuoi,
  },
  {
    id: "3",
    content: "Votrung",
    type: IGarbageType.Huuco,
    image: Huuco.Votrung,
  },
];
const Items2: DragItem[] = [
  {
    id: "4",
    content: "Chai",
    type: IGarbageType.Taiche,
    image: Taiche.chai,
  },
  {
    id: "5",
    content: "Chai nhua",
    type: IGarbageType.Taiche,
    image: Taiche.chainhua,
  },
  {
    id: "6",
    content: "Hop sua",
    type: IGarbageType.Taiche,
    image: Taiche.hopsua,
  },
  {
    id: "7",
    content: "Lon bia",
    type: IGarbageType.Taiche,
    image: Taiche.lonbia,
  },

  {
    id: "9",
    content: "Ly nhua",
    type: IGarbageType.Taiche,
    image: Taiche.lynhua,
  },
  {
    id: "10",
    content: "Rac1",
    type: IGarbageType.Taiche,
    image: Taiche.rac1,
  },
  {
    id: "11",
    content: "Thia",
    type: IGarbageType.Taiche,
    image: Taiche.thia,
  },
];
const Items3: DragItem[] = [
  {
    id: "12",
    content: "Chai do",
    type: IGarbageType.Voco,
    image: Voco.chaido,
  },
  {
    id: "13",
    content: "Chai ruou",
    type: IGarbageType.Voco,
    image: Voco.chairuou,
  },

  {
    id: "15",
    content: "Cuc giay",
    type: IGarbageType.Voco,
    image: Voco.cucgiay,
  },
  {
    id: "16",
    content: "Nilon",
    type: IGarbageType.Voco,
    image: Voco.nilon,
  },

  {
    id: "18",
    content: "Tui rac",
    type: IGarbageType.Voco,
    image: Voco.tuirac,
  },
];

const GarbageWater = () => {
  const garbageItems = [...Items, ...Items2, ...Items3];
  const [items, setItems] = useState<DragItem[]>(garbageItems);
  const [itemDragged, setItemDragged] = useState<DragItem | null>(null);
  const [removeSuccess, setRemoveSuccess] = useState<boolean>(false);
  const onDrop = (typeGarbage: string) => {
    if (!items) return;
    if (typeGarbage === itemDragged?.type) {
      setItems(
        items.filter(
          (item) => item !== itemDragged && item.id !== itemDragged.id
        )
      );
      setRemoveSuccess(true);
    }
  };

  const onDragStart = (item: DragItem) => {
    setItemDragged(item);
  };

  useEffect(() => {
    if (removeSuccess) {
      toast.success("Đúng rồi bé ơi");
      setRemoveSuccess(false);
    }
  }, [removeSuccess]);

  return (
    <div className="background relative">
      <div className="relative top-[260px]">
        <div className="flex justify-center items-center gap-20">
          <div
            onDragOver={(e) => e.preventDefault()}
            onDrop={(e) => onDrop(IGarbageType.Taiche)}
            className="w-[110px] flex flex-col items-center"
          >
            <img src={Taiche.thungractaiche} alt="" />
            <h3 className="text-white">Tái chế</h3>
          </div>
          <div
            className="w-[110px]  flex flex-col items-center"
            onDragOver={(e) => e.preventDefault()}
            onDrop={(e) => onDrop(IGarbageType.Huuco)}
          >
            <img src={Huuco.thungRacHuuCo} alt="" />
            <h3 className="text-white">Hữu cơ</h3>
          </div>
          <div
            className="w-[110px]  flex flex-col items-center "
            onDragOver={(e) => e.preventDefault()}
            onDrop={(e) => onDrop(IGarbageType.Voco)}
          >
            <img src={Voco.thungracvoco} alt="" />
            <h3 className="text-white">Vô cơ</h3>
          </div>
        </div>
      </div>
      <div className="absolute top-[400px] flex flex-wrap gap-2">
        {items.map((item) => {
          function getRandom(min: number, max: number, step: number) {
            return Math.floor(Math.random() * (max - min) + min + step);
          }
          const nums = getRandom(5, 60, 10);

          return (
            <div onDragStart={(e) => onDragStart(item)} draggable>
              <IMGAnimation
                src={item.image}
                className="elementAnimation"
                width={50}
                height={50}
                alt=""
                numRandom={nums}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default GarbageWater;

const IMGAnimation = styled.img<{ numRandom: number }>`
  @keyframes skew {
    0% {
      transform: skewX(${({ numRandom }) => numRandom && `${numRandom}deg`});
    }
    100% {
      transform: skewX(${({ numRandom }) => numRandom && `-${numRandom}deg`});
    }
  }
  animation: skew 3s infinite;
  transform: skew(${({ numRandom }) => numRandom && `${numRandom}deg`});
  animation-direction: alternate-reverse;
`;
