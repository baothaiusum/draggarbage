import React, { useEffect, useState } from "react";
import { IGarbageType } from "../../App";

interface ITrashcanProps {
  onDrop: (prop: IGarbageType) => boolean;
  title: string;
  image: string;
  onSuccess: (isSuccess: boolean) => void;
  garbageType: IGarbageType;
}

const Trashcan: React.FC<ITrashcanProps> = ({
  onDrop,
  image,
  title,
  onSuccess,
  garbageType,
}) => {
  const [isSuccess, setIsSuccess] = useState(false);
  useEffect(() => {
    if (onDrop(garbageType)) {
      setIsSuccess(true);
    }
  }, [onDrop]);

  useEffect(() => {
    if (isSuccess) {
      onSuccess(true);
    }
  }, [isSuccess]);

  return (
    <div
      onDragOver={(e) => e.preventDefault()}
      onDrop={() => {
        onDrop(garbageType);
      }}
      className="w-20 h-20"
    >
      <img src={image} alt="" />
      <h3>{title}</h3>
    </div>
  );
};

export default Trashcan;
