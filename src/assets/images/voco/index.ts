import chaido from "./chaido.png";
import chairuou from "./chairuou.png";
import cucgiay from "./chaithuytinh.png";
import nilon from "./nilon.png";
import thungracvoco from "./thungracvoco.png";
import tuirac from "./tuirac.png";
import chaithuytinh from "./chaithuytinh.png";

export default {
  chaido,
  chairuou,
  cucgiay,
  nilon,
  thungracvoco,
  tuirac,
  chaithuytinh,
};
