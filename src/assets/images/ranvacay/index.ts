import buico from "./buico.png";
import cay from "./cay.png";
import caytrong from "./caytrong.png";
import kechuyen from "./kechuyen.png";
import ran from "./ran.png";
export default {
  buico,
  cay,
  caytrong,
  kechuyen,
  ran,
};
