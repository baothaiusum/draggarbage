import chai from "./chai.png";
import chainhua from "./chainhua.png";
import hopsua from "./hopsua.png";
import lonbia from "./lonbia.png";
import lynhua from "./lynhua.png";
import rac1 from "./rac1.png";
import thia from "./thia.png";
import thungractaiche from "./thungractaiche.png";
export default {
  chai,
  chainhua,
  hopsua,
  lonbia,
  lynhua,
  rac1,
  thia,
  thungractaiche,
};
