export interface IPageItem {
  page: string;
  id: number;
}
export const Pages: IPageItem[] = [
  {
    page: require("./12.png"),
    id: 1,
  },
  {
    page: require("./34.png"),
    id: 2,
  },
  {
    page: require("./56.png"),
    id: 3,
  },
  {
    page: require("./78.png"),
    id: 4,
  },
  {
    page: require("./910.png"),
    id: 5,
  },
  {
    page: require("./1112.png"),
    id: 6,
  },
  {
    page: require("./1314.png"),
    id: 7,
  },
  {
    page: require("./1516.png"),
    id: 8,
  },
  {
    page: require("./1718.png"),
    id: 9,
  },
  {
    page: require("./1920.png"),
    id: 10,
  },
  {
    page: require("./2122.png"),
    id: 11,
  },
  {
    page: require("./2324.png"),
    id: 12,
  },
  {
    page: require("./2526.png"),
    id: 13,
  },
  {
    page: require("./2728.png"),
    id: 14,
  },
  {
    page: require("./2930.png"),
    id: 15,
  },
  {
    page: require("./3132.png"),
    id: 16,
  },
  {
    page: require("./3334.png"),
    id: 17,
  },
  {
    page: require("./3536.png"),
    id: 18,
  },
  {
    page: require("./3738.png"),
    id: 19,
  },
  {
    page: require("./3940.png"),
    id: 20,
  },
];
